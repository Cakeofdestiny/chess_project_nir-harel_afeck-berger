#include "King.h"

King::King(Coord pos, Board* board, color col) : Tool(pos, board, col, KING) {}

King::~King() {}

bool King::canMove(const Coord & target)
{
	return abs(target.getX() - this->_pos.getX()) <= 1 //If it moved at most by one block in each direction, return true.
		&& abs(target.getY() - this->_pos.getY()) <= 1; //Otherwise, it moved too much so we need to return false.;
}
