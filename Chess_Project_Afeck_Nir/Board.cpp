#include "Board.h"

//We include those here to avoid some errors
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

Board::Board() 
{ 
	this->_turn = WHITE;
	this->populateBoard(DEFAULT_BOARD_STRING);	
}

Board::~Board() 
{
	for (int i = 0; i < BOARD_SIDE_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIDE_SIZE; j++)
		{
			if (this->_board[i][j])
			{
				delete this->_board[i][j]; //Free the dynamically allocated memory
				this->_board[i][j] = 0;
			}
		}
	}
}
void Board::moveToolBoard(const Coord& source, const Coord& target, bool isTest) 
{
	Tool *pTool1 = this->getTool(source);
	Tool *pTool2 = this->getTool(target);

	if (pTool2 != NULL && isTest == false)
	{
		capture(target);
		pTool2 = NULL;
	}
	pTool1->move(target, isTest);
	this->_board[target.getX()][target.getY()] = pTool1;
	this->_board[source.getX()][source.getY()] = pTool2;

}
bool Board::promotionPawn(Tool* pawn, const Coord& source,const Coord& target)
{
	bool vaild = false;
	if (pawn->getType() == PAWN || pawn->getType() == PAWN - CAPITAL_OFFSET) //Turn pawn to queen 
	{
		if (target.getX() == 0 || target.getX() == BOARD_SIDE_SIZE - 1)
		{
			delete (pawn);
			pawn = new Queen(target, this, this->_turn);
			this->_board[target.getX()][target.getY()] = pawn;
			this->_board[source.getX()][source.getY()] = 0;
			vaild = true;
		}
	}
	return vaild;
}
string Board::cvtToString() const
{
	string repr;
	for (int i = BOARD_SIDE_SIZE-1; i >= 0; i--)
	{
		for (int j = 0; j < BOARD_SIDE_SIZE; j++)
		{
			if (this->_board[i][j]) //pointer might be 0 if there is no tool there
			{
				repr += (this->_board[i][j])->getType(); //Get the tool in that address, and its type
			}
			else
			{
				repr += NO_TOOL;
			}
		}
	}	
	repr += this->_turn == WHITE ? WHITE_CHAR : BLACK_CHAR;
	return repr; //return the string representation of the board
}

void Board::printBoard() const
{
	char Turn = this->_turn == WHITE ? WHITE_CHAR_PRINT : BLACK_CHAR_PRINT; //What turn is it.
	std::cout << "Turn is: " << Turn << std::endl; //Print the turn
	for (int i = BOARD_SIDE_SIZE - 1; i >= 0; i--)
	{
		for (int j = 0; j  < BOARD_SIDE_SIZE; j++) 
		{
			if(this->_board[i][j] != NULL)
			{
				std::cout << this->_board[i][j]->getType() << " "; //printing
			}
			else 
			{
				std::cout << NO_TOOL << " "; //printing
			}
		}
		std::cout << std::endl;
	}
}

respCode Board::moveTool(const Coord & source, const Coord & target)
{
	Tool* currTool = this->getTool(source);
	respCode checkState = this->performChecks(source, target);

	if (checkState == VALID) //If none of the pre-checks failed
	{
		if (currTool->canMove(target)) //if we can move there
		{
			if (checkMoveForCheck(source, target, this->_turn))
			{
				checkState = INVALID_CAUSES_CHECK;
			}
			else
			{
			    if(!promotionPawn(currTool, source, target)) //If it wasn't promoted, move it
			    {
					moveToolBoard(source, target, false);
			    }

				if (this->checkCheck(this->_turn == WHITE ? BLACK : WHITE)) //perform for the opposite color
				{
					checkState = VALID_CHECK;
					if (this->checkCheckmate()) //checks if it caused a checkmate - no way to escape the check
					{
						checkState = VALID_CHECKMATE;
					}
				}
				else
				{
					checkState = VALID;
				}
			}
				
		}
		else
		{
			checkState = INVALID_MOVEMENT;
		}	
	}
	
	return checkState;
}
Tool* Board::getTool(const Coord& source)
{
	return this->_board[source.getX()][source.getY()];
}

//Toggle the turn of the board
void Board::toggleTurn()
{
	this->_turn = this->_turn == WHITE ? BLACK : WHITE; //If it is white, turn it to black, otherwise, turn to white.
}

//Input: location of the tool to capture
//If there is a tool at that location, this code will free its memory and set it to 0.
void Board::capture(const Coord & location)
{
	if (this->_board[location.getX()][location.getY()]) //if there is a tool there
	{
		delete this->_board[location.getX()][location.getY()]; //free memory
		this->_board[location.getX()][location.getY()] = NULL; //delete tool
	}
}

//I: A 64 char string, that will contain the representation of the board. From bottom left to top right.
void Board::populateBoard(const char* string_rep) 
{
	char current_tool = 0;
	Tool* insertionTool = 0;
	Coord current_Coord;
	color tool_color;

	//Go over all of the characters, and draw the correct tool.
	for (int i = 0; i < BOARD_SIDE_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIDE_SIZE; j++)
		{
			current_tool = string_rep[i * BOARD_SIDE_SIZE + j];
			current_Coord.setX(i);
			current_Coord.setY(j);
			tool_color = WHITE;

			//Switches for the tools
			switch (current_tool)
			{
			case BISHOP:
				tool_color = BLACK; //If the tool is black, change the color and continue to the next line
			case BISHOP - CAPITAL_OFFSET:
				insertionTool = new Bishop(current_Coord, this, tool_color);
				break;

			case KING:
				tool_color = BLACK;
			case KING - CAPITAL_OFFSET:
				insertionTool = new King(current_Coord, this, tool_color);
				break;

			case KNIGHT:
				tool_color = BLACK;
			case KNIGHT - CAPITAL_OFFSET:
				insertionTool = new Knight(current_Coord, this, tool_color);
				break;

			case PAWN:
				tool_color = BLACK;
			case PAWN - CAPITAL_OFFSET:
				insertionTool = new Pawn(current_Coord, this, tool_color);
				break;

			case QUEEN:
				tool_color = BLACK;
			case QUEEN - CAPITAL_OFFSET:
				insertionTool = new Queen(current_Coord, this, tool_color);
				break;

			case ROOK:
				tool_color = BLACK;
			case ROOK - CAPITAL_OFFSET:
				insertionTool = new Rook(current_Coord, this, tool_color);
				break;

			default:
				insertionTool = 0; //If nothing was found, simply insert 0
				break;
			}

			this->_board[i][j] = insertionTool; //actually place the tool in our array
		}
	}
}

bool Board::checkNoToolOnSource(const Coord & source)
{
	Tool* tool = getTool(source);
	bool resp = true;
	if (tool) //if there is a tool there
	{
		if (tool->getColor() == this->_turn)  //and it is of the same color
		{
			resp = false; //the check failed, i.e. it is valid
		}
	}

	return resp; //otherwise, return true
}

bool Board::checkInvalidIndexes(const Coord & source, const Coord & target)
{
	//If either of them are not within the board, return true
	return !source.withinBoard(BOARD_SIDE_SIZE) || !target.withinBoard(BOARD_SIDE_SIZE);
}

bool Board::checkSameCoords(const Coord & source, const Coord & target)
{
	//If they're in the same place, return true
	return source == target;
}

bool Board::checkSameColorOnTarget(const Coord& source, const Coord& target) 
{
	bool resp = false;
	if (this->getTool(target) != NULL)
	{
		resp = this->getTool(target)->getColor() == this->getTool(source)->getColor();
	}
	return resp;
}

bool Board::checkCheck(color checkColor) const
{
	Coord kingIndex = this->findKing(checkColor)->getPos();
	bool isCheck = false;
	
	for (int i = 0; i < BOARD_SIDE_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIDE_SIZE; j++)
		{
			if (this->_board[i][j] != NULL) 
{
				if (this->_board[i][j]->canMove(kingIndex) && this->_board[i][j]->getColor() != checkColor)
				{
					isCheck = true;
					break;
				}
			}
		}
	}
	return isCheck;
}
bool Board::checkCheckmate() 
{
	bool isCheckmate = true; //It is a checkmate until we loop over the tools and discover one that can thwart it.
	this->toggleTurn(); //Toggle the turn instead because it confuses some of the checks
	color kingColor = this->_turn; //We're checking if we caused the other player a checkmate.
	Coord kingIndex = this->findKing(kingColor)->getPos();
	Coord currentCoord;
	Tool* currentTool = 0;

	for (int i = 0; i < BOARD_SIDE_SIZE && isCheckmate; i++) //First two loops go over all of the tools in the board
	{
		for (int j = 0; j < BOARD_SIDE_SIZE && isCheckmate; j++)
		{
			if (this->_board[i][j] != NULL
				&& this->_board[i][j]->getColor() == kingColor) //Check if there's a tool there and if it is the color of the opposing side.
			{
				currentTool = this->_board[i][j]; //gets the ptr of the current tool
				for (int x = 0; x < BOARD_SIDE_SIZE && isCheckmate; x++) //The third and fourth loops check all possible moves for a tool.
				{
					for (int y = 0; y < BOARD_SIDE_SIZE && isCheckmate; y++)
					{
						currentCoord = Coord(x, y);
						if (this->performChecks(currentTool->getPos(), currentCoord) == VALID) //pre-checks performed
						{
							if (currentTool->canMove(currentCoord)) //then if it can actually move there
							{
								//If it returned false, it means that this move prevented a check, so it isn't a checkmate.
								isCheckmate = this->checkMoveForCheck(currentTool->getPos(), currentCoord, kingColor);
							}
						}
						
					}
				}
			}
		}
	}
	this->toggleTurn(); //Return to the previous turn
	return isCheckmate;
}
bool Board::checkMoveForCheck(const Coord& source, const Coord& target, color kingColor) 
{
	//The tool to move
	Tool * temp = this->getTool(source);
	//The target tool 
	Tool * pTarget = this->getTool(target);

	bool result = false; //The return val
						 //Move the tool to check if cause himself to check
	temp->move(target, true);
	this->_board[target.getX()][target.getY()] = temp;
	this->_board[source.getX()][source.getY()] = nullptr;

	result = checkCheck(kingColor);	//Check the check on his king
									//Move the tool back to his place
	temp->move(source, true);
	this->_board[source.getX()][source.getY()] = temp;
	this->_board[target.getX()][target.getY()] = pTarget;

	return result;

}

//Perform all checks in a function.
respCode Board::performChecks(const Coord & source, const Coord & target)
{
	respCode ret = VALID;
	if (this->checkInvalidIndexes(source, target))
	{
		ret = INVALID_INDEXES;
	}
	else if (this->checkNoToolOnSource(source))
	{
		ret = INVALID_NO_TOOL_ON_SOURCE;
	}
	else if (checkSameCoords(source, target))
	{
		ret = INVALID_SAME_COORDS;
	}
	else if (checkSameColorOnTarget(source, target))
	{
		ret = INVALID_SAME_COLOR_ON_TARGET;
	}
	return ret;
}

Tool * Board::findKing(color kingColor) const
{
	char kingRep = kingColor == WHITE ? WHITE_KING: BLACK_KING;
	Tool* kingPtr = 0;
	for (int i = 0; i < BOARD_SIDE_SIZE && !kingPtr; i++)
	{
		for (int j = 0; j < BOARD_SIDE_SIZE && !kingPtr; j++)
		{
			if (this->_board[i][j] != NULL) {
				if (this->_board[i][j]->getType() == kingRep)
				{
					kingPtr = this->_board[i][j];
					break;
				}
			}
		}
	}
	return kingPtr;
}
