#include "Tool.h"

char Tool::getType() const
{
	return this->_type;
}

bool Tool::getFirstMove() const
{
	return this->_firstMove;
}

Tool::Tool(Coord pos, Board* board, color color, char type)
{
	this->_pos = pos;
	this->_board = board;
	this->_color = color;
	this->_type = type;
	this->_firstMove = true;
	if (this->_color == WHITE) 
	{
		this->_type -= CAPITAL_OFFSET;
	}
}

Tool::~Tool() 
{
}

color Tool::getColor() const
{
	return this->_color;
}

Coord Tool::getPos() const
{
	return this->_pos;
}

void Tool::move(const Coord& target,bool isTest) 
{
	if (!isTest) 
	{
		this->_firstMove = false;
	}
	this->_pos = target;
}
