#pragma once

#include "Tool.h"

#include <string>
#include <iostream>
#define BOARD_SIDE_SIZE 8
#define NO_TOOL '#'

#define ROW_2 1
#define WHITE_CHAR '0'
#define BLACK_CHAR '1'

#define CAPITAL_OFFSET 32

#define WHITE_CHAR_PRINT 'W'
#define BLACK_CHAR_PRINT 'B'

#define WHITE_KING 'K'
#define BLACK_KING 'k'
#define DEFAULT_BOARD_STRING "RNBQKBNRPPPPPPPP################################pppppppprnbqkbnr"

using std::string;

typedef enum color color;
class Tool; //for circular references
class Coord;

typedef enum respCode {VALID, VALID_CHECK, INVALID_NO_TOOL_ON_SOURCE, INVALID_SAME_COLOR_ON_TARGET, INVALID_CAUSES_CHECK, INVALID_INDEXES,
INVALID_MOVEMENT, INVALID_SAME_COORDS, VALID_CHECKMATE} respCode;

class Board
{
public:

	Board();
	~Board();

	string cvtToString() const; //convert to string repr from board tool
	void printBoard() const;

	bool checkCheck(color checkColor) const; //Check for check
	bool checkCheckmate(); //Check for checkmate
	
	respCode moveTool(const Coord& source, const Coord& target); //Move the tool and respond with the resp code.

	Tool* getTool(const Coord& source); //get the pointer of the tool at those coordinates

	void toggleTurn(); //this function must be called by the manager to toggle the current turn in the board

	void capture(const Coord& location); //this function will free the memory occupied in the location and change the array to 0
	void populateBoard(const char* string_rep);

	


private:
	Tool*  _board[BOARD_SIDE_SIZE][BOARD_SIDE_SIZE];
	color _turn;

	//CHECK FUNCTIONS
	//Will return true if the check described in their name is positive -- i.e. invalid
	void moveToolBoard(const Coord& source, const Coord& target, bool isTest);

	bool checkNoToolOnSource(const Coord& source);
	bool checkInvalidIndexes(const Coord& source, const Coord& target);
	bool checkSameCoords(const Coord& source, const Coord& target);
	bool checkSameColorOnTarget(const Coord& source, const Coord& target);
	bool promotionPawn(Tool* pawn, const Coord& source,const Coord& target);
	
	Tool* findKing(color kingColor) const;
	respCode performChecks(const Coord& source, const Coord& target);
	bool checkMoveForCheck(const Coord& source, const Coord& target, color kingColor);
};