#include "Pipe.h"
#include "Manager.h"
#include <iostream>
#include <thread>

#define EXIT_OK 0

#define DELAY 2500

#if __linux__
#define CLEAR_CMD "clear"
#else
#define CLEAR_CMD "CLS"
#endif //platform switches

void connectToPipe(Pipe& p);

using namespace std;
void main()
{
	srand(time_t(NULL));
	Pipe p;

	while (true)
	{
		connectToPipe(p);
		Manager manager(p);
		manager.run();
		p.close();
	}
}

//Continue waiting until the pipe is connected
void connectToPipe(Pipe& p)
{
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		system(CLEAR_CMD);
		cout << "I can't connect to the frontend." << endl;
		cout << "Reconnecting..." << endl;
		Sleep(DELAY);
		isConnect = p.connect();
	}
}
