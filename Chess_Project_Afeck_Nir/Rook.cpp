#include "Rook.h"

Rook::Rook(Coord pos, Board* board, color col) : Tool(pos, board, col, ROOK) {}

Rook::~Rook() {}	

bool Rook::canMove(const Coord& target)
{
	bool valid = true;

	if (this->_pos.checkStraight(target)) //Rook move only in straight.
	{
		for (Coord temp : this->_pos.pathTo(target)) //loop over the coordinates
		{
			if (this->_board->getTool(temp) != NULL && temp != target) //checking if tool exists.
			{
				valid = false;
			}
		}
	}
	else
	{
		valid = false;
	}
	return valid;
}
