#pragma once
#include "Tool.h"

#define KING 'k'

class Board;
class Tool;
typedef enum color color;

class King : public Tool
{
public:
	King(Coord pos, Board* board, color col);
	~King();

	virtual bool canMove(const Coord& target); //implementation of pure virtual method
};

