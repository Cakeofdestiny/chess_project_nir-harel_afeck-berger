#pragma once
#include "Tool.h"

#define QUEEN 'q'

class Board;
class Tool;
typedef enum color color;

class Queen : public Tool
{
public:
	Queen(Coord pos, Board* board, color col);
	~Queen();

	virtual bool canMove(const Coord& target); //implementation of pure virtual method
};

