#pragma once
#include "stdafx.h"

#include "Coord.h"
#include "Board.h"

typedef enum color { WHITE, BLACK } color;

class Board;
class Coord;

class Tool
{
public:
	Tool(Coord pos, Board* board, color color, char type);
	~Tool();

	color getColor() const;
	Coord getPos() const;

	//Can move checks if it can move to that position logically, and move actually moves the tool.
	virtual bool canMove(const Coord& target) = 0; //pure virtual as it means nothing in the context of a tool
	void move(const Coord& target, bool isTest);

	char getType() const;
	bool getFirstMove() const;


protected:
	Board* _board;
	Coord _pos;
	color _color;
	char _type;
	bool _firstMove;
};

