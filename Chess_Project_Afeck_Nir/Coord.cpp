#include "Coord.h"

Coord::Coord()
{
	this->_x = 0;
	this->_y = 0;
}

Coord::Coord(int x, int y)
{
	this->_x = x;
	this->_y = y;
}

Coord::~Coord() {}

int Coord::getX() const
{
	return this->_x;
}

int Coord::getY() const
{
	return this->_y;
}

void Coord::setX(int x)
{
	this->_x = x;
}

void Coord::setY(int y)
{
	this->_y = y;
}

bool Coord::operator==(const Coord other) const
{
	return this->_x == other._x && this->_y == other._y;
}

bool Coord::operator!=(const Coord other) const
{
	return !(*this == other);
}

void Coord::printCoord() const
{
	std::cout << "X: " << this->_x << ", Y: " << this->_y << std::endl;
}

//Input: target
//Generates coordinates that are in that path
std::experimental::generator<Coord> Coord::pathTo(const Coord& target) const
{
	Coord curr(*this); //create a curr coord at this position

	int x_direction = 0;
	int y_direction = 0;

	if (target.withinBoard(BOARD_SIDE_SIZE)) //if it is within the board start pathing
	{ 
		if (this->checkStraight(target) || this->checkDiagonal(target)) //either straight or diagonal movement
		{
			//Define movement directions
			if (target._y - curr._y > 0)
			{
				y_direction = 1;
			}
			else if (target._y - curr._y < 0)
			{
				y_direction = -1;
			}

			if (target._x - curr._x > 0)
			{
				x_direction = 1;
			}
			else if (target._x - curr._x < 0)
			{
				x_direction = -1;
			}

			while (curr != target)
			{
				curr._x = curr._x + x_direction;
				curr._y = curr._y + y_direction;
				co_yield curr;
			}
		}  //In any other case, the movement is neither straight nor diagonal, so it is invalid.
	}
}

bool Coord::withinBoard(int side_size) const
{
	//Acceptable range for either x and y : 0<=val<side_size
	return this->_x >= 0 && this->_x < side_size &&
			this->_y >= 0 && this->_y < side_size;
}

bool Coord::checkDiagonal(const Coord & target) const
{
	return abs(target._x - this->_x) == abs(target._y - this->_y); //if movement in both axis is the same, the movement is diagonal.
}

bool Coord::checkStraight(const Coord & target) const
{
	return target._x - this->_x == 0 || target._y - this->_y == 0; //either movement in x or y is 0
}
