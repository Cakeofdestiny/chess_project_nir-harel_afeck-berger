#include "Pawn.h"

Pawn::Pawn(Coord pos, Board* board, color col) : Tool(pos, board, col, PAWN) {}

Pawn::~Pawn() {}

bool Pawn::canMove(const Coord & target)
{
	bool movedForward = false;
	bool valid = false;
	int distance_limit = this->_firstMove ? 2 : 1;

	if (this->_color == WHITE)
	{
		movedForward = target.getX() > this->_pos.getX();
	}
	else
	{
		movedForward = target.getX() < this->_pos.getX(); //Because the direction is different for each color
	}

	if (movedForward)
	{
		if (this->_pos.checkStraight(target)) //Straight movement, without eating
		{
			if (!this->_board->getTool(target)) //If there isn't a tool there we need to check if this is the tool's first move, and then how many blocks it moved.
			{
				if (abs(target.getX() - this->_pos.getX()) <= distance_limit)
				{
					valid = true; //Because it moved straight, and within the distance limit.
				}
			} //Otherwise 'valid' will not be changed.
		}
		else if (this->_pos.checkDiagonal(target)) //It must eat another tool if it is diagonal.
		{
			if (this->_board->getTool(target)) //There must be a tool there for the movement to be legal.
			{
				if (abs(target.getX() - this->_pos.getX()) == 1 && abs(target.getY() - this->_pos.getY()) == 1) //Diagonal movement is 1 in both axes.
				{
					valid = true;
				}
			}
		}
	}

	return valid;
}
