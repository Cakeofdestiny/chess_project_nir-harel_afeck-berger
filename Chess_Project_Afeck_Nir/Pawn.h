#pragma once
#include "Tool.h"

#define PAWN 'p'

class Board;
class Tool;
typedef enum color color;

class Pawn : public Tool
{
public:
	Pawn(Coord pos, Board* board, color col);
	~Pawn();

	virtual bool canMove(const Coord& target); //implementation of pure virtual method
};

