#include "Queen.h"

Queen::Queen(Coord pos, Board* board, color col) : Tool(pos, board, col, QUEEN) {}

Queen::~Queen() {}

bool Queen::canMove(const Coord & target)
{
	bool valid = true;

	if (this->_pos.checkStraight(target) || this->_pos.checkDiagonal(target)) //The queen can move either in a straight direction or diagonally.
	{
		for (Coord temp : this->_pos.pathTo(target)) //loop over the coordinates
		{
			if (this->_board->getTool(temp) != NULL && temp != target) //checking if tool exists in the way of the queen.
			{
				valid = false; //If there's a tool in our way, this move is invalid.
			}
		} 
	}
	else //If this is neither straight nor diagonal, this move is invalid.
	{
		valid = false;
	}
	return valid;
}
