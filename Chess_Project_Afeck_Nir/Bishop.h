#pragma once
#include "Tool.h"

#define BISHOP 'b'

class Board;
class Tool;
typedef enum color color;

class Bishop : public Tool
{
public:
	//c'tor
	Bishop(Coord pos, Board* board, color col);
	~Bishop();

	virtual bool canMove(const Coord& target); //implementation of pure virtual method
};

