#pragma once
#include "Board.h"
#include "Pipe.h"
#include <iostream>


#define START_BOARD_X 'a'
#define START_BOARD_Y '1'

#define ZERO_ASCII_OFFSET '0'

#define GRAPHICS_MSG_MAX_LEN 66

#define QUIT_STRING "quit"

#define INDICE_LENGTH 2

class Manager
{
public:
	Manager(Pipe p);
	~Manager();
	void run(); //start game
private:
	color _turn;
	Board _board; 
	Pipe _p;
	Coord parseCoord(string repr);
	respCode runTurn(string msg); //run the turn using the message from the server, and respond with the resp code

};

