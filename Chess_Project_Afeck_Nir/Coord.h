#pragma once

#include <iostream>
#include <experimental/generator>
#include <cmath>

#define BOARD_SIDE_SIZE 8

class Coord
{
public:
	Coord();
	Coord(int x, int y);
	~Coord();

	//Getters
	int getX() const;
	int getY() const;

	//Setters
	void setX(int x);
	void setY(int y);

	bool operator==(const Coord other) const;
	bool operator!=(const Coord other) const;

	void printCoord() const;

	std::experimental::generator<Coord> pathTo(const Coord& target) const; //This function will return the coordinates in the way to your target, so you can use it to check if there are any people in the way.

	bool withinBoard(const int side_size) const; //returns true if the coord is within the board

	bool checkDiagonal(const Coord& target) const; //these functions fit better here in the context of the coordinate, and not of the context of the tool
	bool checkStraight(const Coord& target) const; 

private:
	int _x;
	int _y;
};

