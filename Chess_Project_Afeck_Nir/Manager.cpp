#include "Manager.h"

Manager::Manager(Pipe p)
{
	this->_p = p;
	this->_turn = WHITE;
}

Manager::~Manager() {}

void Manager::run() //Will run with the pipe until it exits, and then it'll exit itself
{
	char msgToGraphics[GRAPHICS_MSG_MAX_LEN] = { 0 };
	string msgFromGraphics;
	respCode runresp;

	strcpy_s(msgToGraphics, GRAPHICS_MSG_MAX_LEN, this->_board.cvtToString().c_str()); //copy the board representation to the string
	this->_p.sendMessageToGraphics(msgToGraphics);

	msgFromGraphics = this->_p.getMessageFromGraphics();

	while (msgFromGraphics.compare(QUIT_STRING) != 0) //while they're not equal continue performing moves
	{
		runresp = this->runTurn(msgFromGraphics);

		msgToGraphics[0] = runresp + ZERO_ASCII_OFFSET; //add the zero offset for the termination
		msgToGraphics[1] = NULL; //null terminator

		this->_p.sendMessageToGraphics(msgToGraphics); //send the message

		if (runresp == VALID || runresp == VALID_CHECK || runresp == VALID_CHECKMATE) //if it was valid, toggle a turn
		{
			this->_board.toggleTurn();
		}

		msgFromGraphics = this->_p.getMessageFromGraphics();
	}
}


//I: A string containing at least two bytes
//O: a coord object as represented by that string
Coord Manager::parseCoord(string repr)
{
	//We decided to reverse the x and y axes for simplicity
	Coord parsedCoord(repr[1] - START_BOARD_Y, repr[0] - START_BOARD_X); //Build the coord
	return parsedCoord;
}

//I: A representation of the turn, containing at least four bytes
//O: the response code from the board
respCode Manager::runTurn(string msg)
{
	Coord source, target;

	if (msg.length() == INDICE_LENGTH * 2) //make sure it's of the right length
	{
		source = this->parseCoord(msg);
		target = this->parseCoord(msg.substr(INDICE_LENGTH)); //get the second coordinate
	}
	
	return this->_board.moveTool(source, target);
}
