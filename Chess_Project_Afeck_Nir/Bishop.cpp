#include "Bishop.h"

Bishop::Bishop(Coord pos, Board* board, color col) : Tool(pos, board, col, BISHOP) {}

Bishop::~Bishop() {}

bool Bishop::canMove(const Coord& target) {
	bool valid = true;

	if (this->_pos.checkDiagonal(target)) //Bishop move only in diagonal way.
	{
		for (Coord temp : this->_pos.pathTo(target)) //Loop over the coordinates until we get to the target.
		{
			if (this->_board->getTool(temp) != NULL && temp != target) //If we're not at our target, and there's a tool in our way, return false.
			{
				valid = false;
				break;
			}
		}
	}
	else
	{
		valid = false;
	}
	return valid;
}
