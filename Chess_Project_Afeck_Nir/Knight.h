#pragma once
#include "Tool.h"

#define KNIGHT 'n'

class Board;
class Tool;
typedef enum color color;

class Knight : public Tool
{
public:
	Knight(Coord pos, Board* board, color col);
	~Knight();

	virtual bool canMove(const Coord& target); //implementation of pure virtual method
};

