#pragma once

#include "Tool.h"
#define ROOK 'r'

class Board;
class Tool;
typedef enum color color;

class Rook : public Tool
{
public:
	Rook(Coord pos, Board* board, color col);
	~Rook();

	virtual bool canMove(const Coord& target); //implementation of pure virtual method
};

