#include "Knight.h"

Knight::Knight(Coord pos, Board* board, color col) : Tool(pos, board, col, KNIGHT) {}


bool Knight::canMove(const Coord& target)
{
	//Knight can move 2 straight step, and then 1 horizontal step. or 1 straight step, and then 2  horizontal step. forward or backward
	return  abs((target.getX() - this->_pos.getX()) == 1 && abs(target.getY() - this->_pos.getY()) == 2) ||
		    (abs(target.getX() - this->_pos.getX()) == 2 && abs(target.getY() - this->_pos.getY()) == 1) ||
		    (abs(this->_pos.getX() - target.getX()) == 1 && abs(this->_pos.getY() - target.getY()) == 2) ||
		    (abs(this->_pos.getX() - target.getX()) == 2 && abs(this->_pos.getY() - target.getY()) == 1);
	}
